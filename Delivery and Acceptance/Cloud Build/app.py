# Serve model as a flask application

from joblib import load
import numpy as np
from flask import Flask, request

model = None
app = Flask(__name__)


def load_model():
    global model
    # model variable refers to the global variable
    model = load('ChessOutcomePredictor.pkl')


@app.route('/', methods=['GET','POST'])
def home_endpoint():
    return 'Connection Successful!!'


@app.route('/predict', methods=['POST'])
def get_prediction():
    # Works only for a single sample
    if request.method == 'POST':
        data = request.get_data()  
        data = np.fromstring(data,dtype=np.uint16,sep='%2c')  
        data = data.reshape(1,-1)
        prediction = model.predict(data)  
    return str(prediction[0])


if __name__ == '__main__':
    load_model()  
    app.run(host='0.0.0.0', port=80)