﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]
public class ChessPiece : MonoBehaviour
{
    [HideInInspector] public bool m_Tweening = false;
    [HideInInspector] public bool m_MovedOnce = false;
    [HideInInspector] public SpriteRenderer m_Renderer;
    [HideInInspector] public BoxCollider2D m_Collider;
    public PieceData m_Data;
    public ChessBoard m_BoardInst;
    [SerializeField] float m_MoveTime = 1.0f;
    [SerializeField] float m_DiscardTime = 0.5f;
    [SerializeField] string m_ActiveLayer;
    [SerializeField] string m_NormalLayer;
    [HideInInspector] public bool m_Replaced = false;
    void Awake()
    {
        m_BoardInst.m_Pieces[(int)m_Data.m_PosCol, (int)m_Data.m_PosRow] = this;
    }

    void Start()
    {
        m_Renderer = GetComponent<SpriteRenderer>();
        m_Collider = GetComponent<BoxCollider2D>();
    }

    public void MoveToPosition(Row _row, Column _col, ChessPiece _capturedPiece)
    {
        m_BoardInst.m_Pieces[(int)m_Data.m_PosCol, (int)m_Data.m_PosRow] = null;
        if (_capturedPiece != null)
        {
            m_BoardInst.m_Pieces[(int)_capturedPiece.m_Data.m_PosCol, (int)_capturedPiece.m_Data.m_PosRow] = null;
        }
        string fromPoint = m_Data.GetPositionAsStr();
        m_Data.m_PosCol = _col;
        m_Data.m_PosRow = _row;
        string toPoint = m_Data.GetPositionAsStr();
        if (m_BoardInst.m_GameMoves.Count < 20)
        {
            m_BoardInst.m_GameMoves.Add((fromPoint + "," + toPoint));
        }
        m_BoardInst.m_Pieces[(int)m_Data.m_PosCol, (int)m_Data.m_PosRow] = this;
        m_MovedOnce = true;
        LeanTween.move(gameObject, m_BoardInst.m_BoardPositions[(int)m_Data.m_PosCol, (int)m_Data.m_PosRow], m_MoveTime).setDelay(0.1f).setEaseInQuad()
            .setOnStart(() =>
            {
                m_Tweening = true;
                m_Renderer.sortingLayerName = m_ActiveLayer;
            })
            .setOnComplete(() =>
            {
                m_Tweening = false;
                m_Renderer.sortingLayerName = m_NormalLayer;
                if (_capturedPiece != null)
                {
                    _capturedPiece.GetCapturedAndFadeAway();
                }
            });
    }

    public void GetCapturedAndFadeAway()
    {
        LeanTween.move(gameObject, m_BoardInst.m_DiscardPos[(int)m_Data.m_Color].position, m_DiscardTime).setEaseOutExpo()
            .setOnStart(() => m_Tweening = true).setOnComplete(() =>
            {
                LeanTween.value(gameObject, 1, 0, m_DiscardTime).setOnComplete
                (() => Destroy(gameObject));
            });
    }

    public void ChangePieceType(Piece _piece, Sprite _sprite)
    {
        m_BoardInst.m_Input = true;
        m_Renderer.sprite = _sprite;
        m_Data.m_Type = _piece;
    }

}
