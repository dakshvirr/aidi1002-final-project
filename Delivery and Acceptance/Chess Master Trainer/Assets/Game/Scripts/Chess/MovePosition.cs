﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum MoveType
{
    Empty,
    OpponentCapture,
    Danger
}

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]
public class MovePosition : MonoBehaviour
{
    [HideInInspector] public Row m_PosRow;
    [HideInInspector] public Column m_PosCol;
    [HideInInspector] public SpriteRenderer m_Renderer;
    [HideInInspector] public BoxCollider2D m_Collider;
    [HideInInspector] public MoveType m_Type = MoveType.Empty;
    [SerializeField] Color[] m_Colors;
    [HideInInspector] public ChessPiece m_PieceToBeCut;
    [HideInInspector] public ChessPiece m_CastlingKing;
    [HideInInspector] public ChessPiece m_CastlingRook;
    void Start()
    {
        Setup();
    }

    void Setup()
    {
        if (m_Renderer == null)
        {
            m_Renderer = GetComponent<SpriteRenderer>();
            m_Collider = GetComponent<BoxCollider2D>();
        }
    }

    void OnEnable()
    {
        Setup();
        m_Renderer.color = m_Colors[(int)m_Type];
    }

}
