﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using UnityEngine;
#region Definitions
[Serializable]
public enum Piece
{
    Pawn,
    Rook,
    Knight,
    Bishop,
    Queen,
    King
}

[Serializable]
public enum Side
{
    None = -1,
    White,
    Black
}
[Serializable]
public enum Column { a, b, c, d, e, f, g, h }
[Serializable]
public enum Row { r1, r2, r3, r4, r5, r6, r7, r8 }

[Serializable]
public class PieceData
{
    public Piece m_Type;
    public Side m_Color;
    public Column m_PosCol;
    public Row m_PosRow;

    public string GetPositionAsStr()
    {
        return m_PosCol.ToString() + ((int)m_PosRow + 1).ToString();
    }
}
[Serializable]
public struct PositionData
{
    public Row m_Row;
    public Column m_Column;
}
#endregion
public class ChessBoard : MonoBehaviour
{
    [HideInInspector] public Camera m_MainCamera;
    [SerializeField] GameObject m_PiecePrefab;
    [SerializeField] GameObject m_MovePointPrefab;
    [SerializeField] Color[] m_PieceColor;
    [SerializeField] Transform[] m_PieceParent;
    [SerializeField] Vector2 m_BoardOffset;
    [SerializeField] Transform m_StartPos;
    [SerializeField] LayerMask m_InteractableMask;
    [SerializeField] MenuClassifier m_HUDMenu;
    [SerializeField] MenuClassifier m_PawnSelector;
    public Transform[] m_DiscardPos;
    [HideInInspector] public readonly Vector3[,] m_BoardPositions = new Vector3[8, 8];
    [HideInInspector] public readonly ChessPiece[,] m_Pieces = new ChessPiece[8, 8];
    [HideInInspector] public readonly List<string> m_GameMoves = new List<string>();
    readonly List<MovePosition> m_Moveables = new List<MovePosition>();
    [HideInInspector] public Side m_Chance = Side.White;
    ChessPiece m_SelectedPiece = null;
    bool[] m_PreviouslyInCheck = new bool[2];
    HUDMenu m_MenuInst;
    Side m_InCheck = Side.None;
    Side m_Predicted = Side.None;
    bool m_Requested = false;
    [HideInInspector] public PawnSelectorMenu m_SelectorMenu;
    [HideInInspector] public bool m_Input = true;

    void Awake()
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                m_Pieces[i, j] = null;
            }
        }
        m_Input = true;
        MenuManager.Instance.ShowMenu(m_HUDMenu);
        m_MenuInst = MenuManager.Instance.GetMenu<HUDMenu>(m_HUDMenu);
        m_SelectorMenu = MenuManager.Instance.GetMenu<PawnSelectorMenu>(m_PawnSelector);
        UpdateHUD();
    }

    void UpdateHUD()
    {
        m_MenuInst.m_ChanceTxt.text = "CHANCE\n" + m_Chance.ToString();
        m_MenuInst.m_CheckTxt.text = "CHECK\n" + m_InCheck.ToString();
        m_MenuInst.m_PredictionTxt.text = m_Predicted.ToString();
    }

    void OnDestroy()
    {
        if (MenuManager.IsValidSingleton())
        {
            MenuManager.Instance.HideMenu(m_HUDMenu);
        }
    }

    void Start()
    {
        m_MainCamera = Camera.main;
        PopulatePositionDictionary();
    }

    void PopulatePositionDictionary()
    {
        Vector3 startPos = m_StartPos.position;
        Vector3 offset = Vector3.zero;
        foreach (var column in Enum.GetValues(typeof(Column)))
        {
            foreach (var row in Enum.GetValues(typeof(Row)))
            {
                m_BoardPositions[(int)column, (int)row] = startPos + offset;
                offset.y += m_BoardOffset.y;
            }
            offset.y = 0;
            offset.x += m_BoardOffset.x;
        }
    }

    void Update()
    {
        if (!m_Input)
        {
            return;
        }
        if (Input.GetMouseButtonUp(0))
        {
            Vector3 mousePosition = m_MainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
            mousePosition.z = m_MainCamera.transform.position.z;
            RaycastHit2D hit = Physics2D.Raycast(mousePosition, Vector3.forward, 50.0f, m_InteractableMask);
            if (hit.collider != null)
            {
                ChessPiece piece = hit.collider.GetComponent<ChessPiece>();
                if (piece != null)
                {
                    if (m_SelectedPiece == null)
                    {
                        if (piece.m_Data.m_Color != m_Chance)
                        {
                            return;
                        }
                        else if (!piece.m_Tweening)
                        {
                            m_SelectedPiece = piece;
                            ShowMoveableBlocks();
                        }
                    }
                    else if (piece == m_SelectedPiece)
                    {
                        m_SelectedPiece = null;
                        RemoveMoveables();
                    }
                }
                else if (m_SelectedPiece != null)
                {
                    //find if mover present if yes then move
                    MovePosition mover = hit.collider.GetComponent<MovePosition>();
                    if (mover == null)
                    {
                        return;
                    }
                    if (mover.m_Type == MoveType.Danger) //can't move if king in check
                    {
                        return;
                    }
                    HandlePieceMovement(mover);
                }
            }
        }
    }

    void HandlePieceMovement(MovePosition _mover)
    {
        Row row = _mover.m_PosRow;
        Column col = _mover.m_PosCol;
        ChessPiece capturedPiece = null;
        if (_mover.m_Type == MoveType.OpponentCapture)//capture opponent
        {
            capturedPiece = _mover.m_PieceToBeCut;
        }
        if (_mover.m_CastlingRook != null) //perform castling
        {
            PerformKingCastling(_mover.m_CastlingKing, _mover.m_CastlingRook);
        }
        else
        {
            m_SelectedPiece.MoveToPosition(row, col, capturedPiece);
        }
        if (m_SelectedPiece.m_Data.m_Type == Piece.Pawn && !m_SelectedPiece.m_Replaced)
        {
            Row checkRow = m_Chance == Side.White ? Row.r8 : Row.r1;
            if (m_SelectedPiece.m_Data.m_PosRow == checkRow)
            {
                m_SelectedPiece.m_Replaced = true;
                m_SelectorMenu.m_Caller = m_SelectedPiece;
                m_SelectorMenu.SetupMenu(m_Chance);
                m_Input = false;
            }
        }
        m_SelectedPiece = null;
        m_Chance = (Side)(((int)m_Chance + 1) % 2);
        RemoveMoveables();
        EvaluateBoard();
    }

    void PerformKingCastling(ChessPiece _king, ChessPiece _rook)
    {
        int colVal;
        if ((int)_king.m_Data.m_PosCol > (int)_rook.m_Data.m_PosCol)//queen side
        {
            colVal = (int)_king.m_Data.m_PosCol - 2;
            _king.MoveToPosition(_king.m_Data.m_PosRow, (Column)colVal, null);
            colVal += 1;
            _rook.MoveToPosition(_king.m_Data.m_PosRow, (Column)colVal, null);
        }
        else//king side
        {
            colVal = (int)_king.m_Data.m_PosCol + 2;
            _king.MoveToPosition(_king.m_Data.m_PosRow, (Column)colVal, null);
            colVal -= 1;
            _rook.MoveToPosition(_king.m_Data.m_PosRow, (Column)colVal, null);
        }

    }

    void EvaluateBoard()
    {
        List<PositionData> possibleBlackMoves = GetAllMoves(Side.Black);
        List<PositionData> possibleWhiteMoves = GetAllMoves(Side.White);
        ChessPiece blackKing = GetKing(Side.Black);
        ChessPiece whiteKing = GetKing(Side.White);
        if (possibleBlackMoves.Any(move => move.m_Row == whiteKing.m_Data.m_PosRow && move.m_Column == whiteKing.m_Data.m_PosCol))
        {
            if (m_PreviouslyInCheck[(int)Side.White])
            {
                GameManager.Instance.EndGame(Side.Black, m_Predicted);
            }
            else
            {
                m_PreviouslyInCheck[(int)Side.White] = true;
                m_InCheck = Side.White;
            }
        }
        else
        {
            m_PreviouslyInCheck[(int)Side.White] = false;
        }
        if (possibleWhiteMoves.Any(move => move.m_Row == blackKing.m_Data.m_PosRow && move.m_Column == blackKing.m_Data.m_PosCol))
        {
            if (m_PreviouslyInCheck[(int)Side.Black])
            {
                GameManager.Instance.EndGame(Side.White, m_Predicted);
            }
            else
            {
                m_PreviouslyInCheck[(int)Side.Black] = true;
                m_InCheck = Side.Black;
            }
        }
        else
        {
            m_PreviouslyInCheck[(int)Side.Black] = false;
        }
        UpdateHUD();
        if (!m_Requested && m_GameMoves.Count <= 20)
        {
            StartCoroutine(GetPrediction());
        }
    }

    IEnumerator GetPrediction()
    {
        if (m_Requested)
        {
            yield break;
        }
        m_Requested = true;
        string data = GetTargetFeatures();
        WebRequestManager.Instance.SendPostRequest("/predict", data);
        yield return new WaitWhile(() => WebRequestManager.Instance.m_Running);
        int predictedVal = -1;
        if (!int.TryParse(WebRequestManager.Instance.m_Result, out predictedVal))
        {
            m_Requested = false;
            yield break;
        }
        if (predictedVal == 0)
        {
            m_Predicted = Side.Black;
        }
        else if (predictedVal == 1)
        {
            m_Predicted = Side.White;
        }
        else
        {
            m_Predicted = Side.None;
        }
        UpdateHUD();
        m_Requested = false;
    }

    ChessPiece GetKing(Side _color)
    {
        foreach (ChessPiece piece in m_Pieces)
        {
            if (piece == null)
            {
                continue;
            }
            if (piece.m_Data.m_Type == Piece.King && piece.m_Data.m_Color == _color)
            {
                return piece;
            }
        }
        return null;
    }
    List<PositionData> GetAllMoves(Side _color)
    {
        List<PositionData> allMoves = new List<PositionData>();
        foreach (ChessPiece piece in m_Pieces)
        {
            if (piece == null)
            {
                continue;
            }
            if (piece.m_Data.m_Type != Piece.King && piece.m_Data.m_Color == _color)
            {
                allMoves.AddRange(GetPieceMoves(piece));
            }
        }
        return allMoves;
    }
    List<PositionData> GetPieceMoves(ChessPiece _piece)
    {
        List<PositionData> allMoves = new List<PositionData>();
        switch (_piece.m_Data.m_Type)
        {
            case Piece.Pawn:
                InstantiateCapturePawnMoves(_piece, allMoves);
                break;
            case Piece.Rook:
                InstantiateRookNormalMoves(_piece, allMoves);
                break;
            case Piece.Bishop:
                InstantiateBishopNormalMoves(_piece, allMoves);
                break;
            case Piece.Knight:
                InstantiateKnightNormalMoves(_piece, allMoves);
                break;
            case Piece.Queen:
                InstantiateRookNormalMoves(_piece, allMoves);
                InstantiateBishopNormalMoves(_piece, allMoves);
                break;
            case Piece.King:
                InstantiateKingNormalMoves(_piece, allMoves);
                break;
        }
        return allMoves;
    }

    #region Moving Tables
    void ShowMoveableBlocks()
    {
        if (m_SelectedPiece == null)
        {
            return;
        }
        switch (m_SelectedPiece.m_Data.m_Type)
        {
            case Piece.Pawn:
                InstantiateOneStepPawnMoves();
                InstantiateTwoStepPawnMoves();
                InstantiateCapturePawnMoves(m_SelectedPiece);
                InstantiateEnPassantPawnMoves();
                break;
            case Piece.Rook:
                InstantiateCastling();
                InstantiateRookNormalMoves(m_SelectedPiece);
                break;
            case Piece.Bishop:
                InstantiateBishopNormalMoves(m_SelectedPiece);
                break;
            case Piece.Knight:
                InstantiateKnightNormalMoves(m_SelectedPiece);
                break;
            case Piece.Queen:
                InstantiateRookNormalMoves(m_SelectedPiece);
                InstantiateBishopNormalMoves(m_SelectedPiece);
                break;
            case Piece.King:
                InstantiateCastling();
                InstantiateKingNormalMoves(m_SelectedPiece);
                break;
        }
        if (m_Moveables.Count == 0)
        {
            m_SelectedPiece = null;
        }
    }
    #region King Move Table
    void InstantiateKingNormalMoves(ChessPiece _pieceToCheck, List<PositionData> _allMoves = null)
    {
        for (int colVal = (int)_pieceToCheck.m_Data.m_PosCol - 1; colVal <= (int)_pieceToCheck.m_Data.m_PosCol + 1; colVal++)
        {
            for (int rowVal = (int)_pieceToCheck.m_Data.m_PosRow - 1; rowVal <= (int)_pieceToCheck.m_Data.m_PosRow + 1; rowVal++)
            {
                if (colVal == (int)_pieceToCheck.m_Data.m_PosCol && rowVal == (int)_pieceToCheck.m_Data.m_PosRow)
                {
                    continue;
                }
                if (IsIndexIdentified(rowVal, colVal))
                {
                    if (_allMoves == null)
                    {
                        InstantiateMovableAndSetType(rowVal, colVal);
                    }
                    else
                    {
                        ChessPiece piece = m_Pieces[colVal, rowVal];
                        if (IsOpponentPiece(piece, _pieceToCheck.m_Data.m_Color))
                        {
                            _allMoves.Add(new PositionData
                            {
                                m_Column = (Column)colVal,
                                m_Row = (Row)rowVal
                            });
                        }
                    }
                }
            }
        }
    }
    #endregion
    #region Pawn Move Table
    void InstantiateOneStepPawnMoves()
    {
        int rowVal = (int)m_SelectedPiece.m_Data.m_PosRow + (m_Chance == Side.White ? 1 : -1);
        if (!Enum.IsDefined(typeof(Row), rowVal)) //out of rows
        {
            return;
        }
        if (m_Pieces[(int)m_SelectedPiece.m_Data.m_PosCol, rowVal] != null)//someone exists there
        {
            return;
        }
        CreateMoveable((Row)rowVal, m_SelectedPiece.m_Data.m_PosCol, MoveType.Empty, null);
    }
    void InstantiateTwoStepPawnMoves()
    {
        if (m_SelectedPiece.m_MovedOnce)
        {
            return;
        }
        int rowVal = (int)m_SelectedPiece.m_Data.m_PosRow + (m_Chance == Side.White ? 2 : -2);
        if (!Enum.IsDefined(typeof(Row), rowVal)) //out of rows
        {
            return;
        }
        if (m_Pieces[(int)m_SelectedPiece.m_Data.m_PosCol, rowVal] != null)//someone exists there
        {
            return;
        }
        CreateMoveable((Row)rowVal, m_SelectedPiece.m_Data.m_PosCol, MoveType.Empty, null);
    }
    void InstantiateCapturePawnMoves(ChessPiece _pieceToCheck, List<PositionData> _allMoves = null)
    {
        int rowVal = (int)_pieceToCheck.m_Data.m_PosRow + (m_Chance == Side.White ? 1 : -1);
        if (!Enum.IsDefined(typeof(Row), rowVal)) //out of rows
        {
            return;
        }
        int colValL = (int)_pieceToCheck.m_Data.m_PosCol - 1;
        if (Enum.IsDefined(typeof(Column), colValL)) //only check if left column exists
        {
            ChessPiece leftPiece = m_Pieces[colValL, rowVal];
            if (IsOpponentPiece(leftPiece, _pieceToCheck.m_Data.m_Color))
            {
                if (_allMoves == null)
                {
                    CreateMoveable((Row)rowVal, (Column)colValL, MoveType.OpponentCapture, leftPiece);
                }
                else
                {
                    _allMoves.Add(new PositionData
                    {
                        m_Row = (Row)rowVal,
                        m_Column = (Column)colValL
                    });
                }
            }
        }
        int colValR = (int)_pieceToCheck.m_Data.m_PosCol + 1;
        if (Enum.IsDefined(typeof(Column), colValR)) //only check if right column exists
        {
            ChessPiece rightPiece = m_Pieces[colValR, rowVal];
            if (IsOpponentPiece(rightPiece, _pieceToCheck.m_Data.m_Color))
            {
                if (_allMoves == null)
                {
                    CreateMoveable((Row)rowVal, (Column)colValR, MoveType.OpponentCapture, rightPiece);
                }
                else
                {
                    _allMoves.Add(new PositionData
                    {
                        m_Row = (Row)rowVal,
                        m_Column = (Column)colValR
                    });

                }
            }
        }
    }

    void InstantiateEnPassantPawnMoves()
    {
        if (m_Chance == Side.Black && m_SelectedPiece.m_Data.m_PosRow != Row.r4)
        {
            return;
        }
        else if (m_Chance == Side.White && m_SelectedPiece.m_Data.m_PosRow != Row.r5)
        {
            return;
        }
        int rowVal = (int)m_SelectedPiece.m_Data.m_PosRow + (m_Chance == Side.White ? 1 : -1); //for the move position
        int colValL = (int)m_SelectedPiece.m_Data.m_PosCol - 1;
        if (Enum.IsDefined(typeof(Column), colValL)) //only check if left column exists
        {
            ChessPiece leftPiece = m_Pieces[colValL, (int)m_SelectedPiece.m_Data.m_PosRow];
            if (IsOpponentPiece(leftPiece, m_Chance) && m_Pieces[colValL, rowVal] == null) //adjacent is opponent and move point is empty
            {
                CreateMoveable((Row)rowVal, (Column)colValL, MoveType.OpponentCapture, leftPiece);
            }
        }
        int colValR = (int)m_SelectedPiece.m_Data.m_PosCol + 1;
        if (Enum.IsDefined(typeof(Column), colValR)) //only check if right column exists
        {
            ChessPiece rightPiece = m_Pieces[colValR, (int)m_SelectedPiece.m_Data.m_PosRow];
            if (IsOpponentPiece(rightPiece, m_Chance) && m_Pieces[colValR, rowVal] == null) //adjacent is opponent and move point is empty
            {
                CreateMoveable((Row)rowVal, (Column)colValL, MoveType.OpponentCapture, rightPiece);
            }
        }
    }
    #endregion
    #region Rook Move Table
    void InstantiateRookNormalMoves(ChessPiece _pieceToCheck, List<PositionData> _allMoves = null)
    {
        int rowVal = (int)_pieceToCheck.m_Data.m_PosRow;
        int colVal = (int)_pieceToCheck.m_Data.m_PosCol;
        #region Row Wise Moves
        CheckAll(1, 0, rowVal, colVal, _pieceToCheck, _allMoves);
        CheckAll(-1, 0, rowVal, colVal, _pieceToCheck, _allMoves);
        #endregion
        #region Column Wise Moves
        CheckAll(0, 1, rowVal, colVal, _pieceToCheck, _allMoves);
        CheckAll(0, -1, rowVal, colVal, _pieceToCheck, _allMoves);
        #endregion
    }
    #endregion
    #region Bishop Move Table
    void InstantiateBishopNormalMoves(ChessPiece _pieceToCheck, List<PositionData> _allMoves = null)
    {
        int rowVal = (int)_pieceToCheck.m_Data.m_PosRow;
        int colVal = (int)_pieceToCheck.m_Data.m_PosCol;
        #region Q1
        CheckAll(1, 1, rowVal, colVal, _pieceToCheck, _allMoves);
        #endregion
        #region Q2
        CheckAll(1, -1, rowVal, colVal, _pieceToCheck, _allMoves);
        #endregion
        #region Q3
        CheckAll(-1, 1, rowVal, colVal, _pieceToCheck, _allMoves);
        #endregion
        #region Q4
        CheckAll(-1, -1, rowVal, colVal, _pieceToCheck, _allMoves);
        #endregion
    }
    #endregion
    #region Knight Move Table
    void CheckAndInstantiateForKnight(ChessPiece _pieceToCheck, List<PositionData> _allMoves, int _checkRow, int _checkCol)
    {
        if (IsIndexIdentified(_checkRow, _checkCol))
        {
            if (_allMoves == null)
            {
                InstantiateMovableAndSetType(_checkRow, _checkCol);
            }
            else
            {
                ChessPiece piece = m_Pieces[_checkCol, _checkRow];
                if (IsOpponentPiece(piece, _pieceToCheck.m_Data.m_Color))
                {
                    _allMoves.Add(new PositionData
                    {
                        m_Column = (Column)_checkCol,
                        m_Row = (Row)_checkRow
                    });
                }
            }
        }

    }
    void InstantiateKnightNormalMoves(ChessPiece _pieceToCheck, List<PositionData> _allMoves = null)
    {
        int rowVal = (int)_pieceToCheck.m_Data.m_PosRow;
        int colVal = (int)_pieceToCheck.m_Data.m_PosCol;
        int checkRow;
        int checkCol;
        #region Q1
        checkCol = colVal + 2;
        checkRow = rowVal + 1;
        CheckAndInstantiateForKnight(_pieceToCheck, _allMoves, checkRow, checkCol);
        checkCol = colVal + 1;
        checkRow = rowVal + 2;
        CheckAndInstantiateForKnight(_pieceToCheck, _allMoves, checkRow, checkCol);
        #endregion
        #region Q2
        checkCol = colVal - 2;
        checkRow = rowVal + 1;
        CheckAndInstantiateForKnight(_pieceToCheck, _allMoves, checkRow, checkCol);
        checkCol = colVal + 1;
        checkRow = rowVal - 2;
        CheckAndInstantiateForKnight(_pieceToCheck, _allMoves, checkRow, checkCol);
        #endregion
        #region Q3
        checkCol = colVal - 2;
        checkRow = rowVal - 1;
        CheckAndInstantiateForKnight(_pieceToCheck, _allMoves, checkRow, checkCol);
        checkCol = colVal - 1;
        checkRow = rowVal - 2;
        CheckAndInstantiateForKnight(_pieceToCheck, _allMoves, checkRow, checkCol);
        #endregion
        #region Q4
        checkCol = colVal + 2;
        checkRow = rowVal - 1;
        CheckAndInstantiateForKnight(_pieceToCheck, _allMoves, checkRow, checkCol);
        checkCol = colVal - 1;
        checkRow = rowVal + 2;
        CheckAndInstantiateForKnight(_pieceToCheck, _allMoves, checkRow, checkCol);
        #endregion
    }
    #endregion
    #region Castling King and Rook
    void InstantiateCastling()
    {
        if (m_SelectedPiece.m_MovedOnce)
        {
            return;
        }
        switch (m_SelectedPiece.m_Data.m_Type)
        {
            case Piece.King:
                int colVal = (int)m_SelectedPiece.m_Data.m_PosCol + 3;
                ChessPiece rook = m_Pieces[colVal, (int)m_SelectedPiece.m_Data.m_PosRow];
                if (rook != null)
                {
                    if (!rook.m_MovedOnce)
                    {
                        for (int i = colVal - 1; i > colVal - 3; i--)
                        {
                            if (m_Pieces[i, (int)m_SelectedPiece.m_Data.m_PosRow] != null)
                            {
                                break;
                            }
                        }
                    }
                }
                InstantiateCastling(m_SelectedPiece, rook); //king side
                colVal = 0;
                rook = m_Pieces[colVal, (int)m_SelectedPiece.m_Data.m_PosRow];
                if (rook != null)
                {
                    if (!rook.m_MovedOnce)
                    {
                        for (int i = colVal + 1; i < colVal + 4; i++)
                        {
                            if (m_Pieces[i, (int)m_SelectedPiece.m_Data.m_PosRow] != null)
                            {
                                break;
                            }
                        }
                    }
                }
                InstantiateCastling(m_SelectedPiece, rook); //queen side
                break;
            case Piece.Rook:
                if ((int)m_SelectedPiece.m_Data.m_PosCol <= 3) //check queen side castling
                {
                    int colValR = (int)m_SelectedPiece.m_Data.m_PosCol + 4;
                    ChessPiece king = m_Pieces[colValR, (int)m_SelectedPiece.m_Data.m_PosRow];
                    if (king == null)
                    {
                        return;
                    }
                    if (king.m_MovedOnce)
                    {
                        return;
                    }
                    for (int i = colValR - 1; i > colValR - 4; i--)
                    {
                        if (m_Pieces[i, (int)m_SelectedPiece.m_Data.m_PosRow] != null)
                        {
                            return;
                        }
                    }
                    InstantiateCastling(king, m_SelectedPiece);
                }
                else //check king side castling
                {
                    int colValR = (int)m_SelectedPiece.m_Data.m_PosCol - 3;
                    ChessPiece king = m_Pieces[colValR, (int)m_SelectedPiece.m_Data.m_PosRow];
                    if (king == null)
                    {
                        return;
                    }
                    if (king.m_MovedOnce)
                    {
                        return;
                    }
                    for (int i = colValR + 1; i < colValR + 3; i++)
                    {
                        if (m_Pieces[i, (int)m_SelectedPiece.m_Data.m_PosRow] != null)
                        {
                            return;
                        }
                    }
                    InstantiateCastling(king, m_SelectedPiece);
                }
                break;
        }
    }

    void InstantiateCastling(ChessPiece _king, ChessPiece _rook)
    {
        int colVal;
        if ((int)_king.m_Data.m_PosCol > (int)_rook.m_Data.m_PosCol)//queen side
        {
            colVal = (int)_king.m_Data.m_PosCol - 2;
            if (m_SelectedPiece != _king)
            {
                colVal += 1;
            }
        }
        else//king side
        {
            colVal = (int)_king.m_Data.m_PosCol + 2;
            if (m_SelectedPiece != _king)
            {
                colVal -= 1;
            }
        }
        CreateMoveable(m_SelectedPiece.m_Data.m_PosRow, (Column)colVal, MoveType.Empty, null, _king, _rook);
    }

    #endregion
    #endregion
    #region Helpers
    bool IsIndexIdentified(int _row, int _col)
    {
        return (_row < m_Pieces.GetLength(1) && _row >= 0) && (_col < m_Pieces.GetLength(0) && _col >= 0);
    }
    void CheckAll(int _incRow, int _incCol, int _rowVal, int _colVal, ChessPiece _pieceToCheck, List<PositionData> _allMoves)
    {
        int incCol = _colVal + _incCol;
        int incRow = _rowVal + _incRow;
        while (IsIndexIdentified(incRow, incCol))
        {
            if (_allMoves == null)
            {
                if (InstantiateMovableAndSetType(incRow, incCol))
                {
                    break;
                }
            }
            else
            {
                ChessPiece piece = m_Pieces[incCol, incRow];
                if (IsOpponentPiece(piece, _pieceToCheck.m_Data.m_Color))
                {
                    _allMoves.Add(new PositionData
                    {
                        m_Column = (Column)incCol,
                        m_Row = (Row)incRow
                    });
                    break;
                }
            }
            incRow += _incRow;
            incCol += _incCol;
        }

    }
    bool InstantiateMovableAndSetType(int _row, int _col)
    {
        ChessPiece piece = m_Pieces[_col, _row];
        if (piece != null)
        {
            if (IsOpponentPiece(piece, m_Chance))
            {
                CreateMoveable((Row)_row, (Column)_col, MoveType.OpponentCapture, piece);
            }
            return true;
        }
        CreateMoveable((Row)_row, (Column)_col, MoveType.Empty, null);
        return false;
    }

    void RemoveMoveables()
    {
        foreach (var movable in m_Moveables)
        {
            Destroy(movable.gameObject);
        }
        m_Moveables.Clear();
    }
    void CreateMoveable(Row _row, Column _column, MoveType _type, ChessPiece _pieceToBeCut, ChessPiece _castlingKing = null, ChessPiece _castlingRook = null)
    {
        if (m_Moveables.Any(move => move.m_PosRow == _row && move.m_PosCol == _column))//already exists
        {
            return;
        }
        GameObject movePos = Instantiate(m_MovePointPrefab, m_PieceParent[(int)m_Chance], false);
        movePos.transform.position = m_BoardPositions[(int)_column, (int)_row];
        MovePosition moveInst = movePos.GetComponent<MovePosition>();
        moveInst.m_PosCol = _column;
        moveInst.m_PosRow = _row;
        moveInst.m_Type = _type;
        moveInst.m_PieceToBeCut = _pieceToBeCut;
        moveInst.m_CastlingKing = _castlingKing;
        moveInst.m_CastlingRook = _castlingRook;
        movePos.SetActive(true);
        m_Moveables.Add(moveInst);
    }

    bool IsOpponentPiece(ChessPiece _piece, Side _side)
    {
        if (_piece == null)
        {
            return false;
        }
        return _piece.m_Data.m_Color != _side;
    }

    string GetTargetFeatures()
    {
        OrderedDictionary tempValDict = new OrderedDictionary();
        StringBuilder finalInput = new StringBuilder();
        tempValDict.Add("white_rating", GameManager.Instance.m_WhiteRating);
        tempValDict.Add("black_rating", GameManager.Instance.m_BlackRating);
        for (int i = 1; i < 21; i++)
        {
            foreach (var col in Enum.GetValues(typeof(Column)))
            {
                foreach (var row in Enum.GetValues(typeof(Row)))
                {
                    string rowval = col.ToString() + ((int)row + 1).ToString();
                    tempValDict.Add("Move " + i.ToString() + "_from_" + rowval, 0);
                    tempValDict.Add("Move " + i.ToString() + "_to_" + rowval, 0);
                }
            }
        }
        for (int i = 0; i < m_GameMoves.Count; i++)
        {
            string[] splt = m_GameMoves[i].Split(',');
            tempValDict["Move " + (i + 1).ToString() + "_from_" + splt[0]] = 1;
            tempValDict["Move " + (i + 1).ToString() + "_to_" + splt[1]] = 1;
        }
        List<int> values = new List<int>();
        foreach (var value in tempValDict.Values)
        {
            values.Add((int)value);
        }
        values.Reverse();
        foreach (var value in values)
        {
            finalInput.Append(value.ToString() + ",");
        }
        finalInput.Length--;
        return finalInput.ToString();
    }

    #endregion

}
