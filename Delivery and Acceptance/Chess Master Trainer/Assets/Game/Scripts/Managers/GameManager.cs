﻿///-------------------------------------------------------------------------------------------------
// File: GameManager.cs
//
// Author: Dakshvir Singh Rehill
// Date: 22/6/2020
//
// Summary:	Manager responsible for the game's transition i.e. circle of life
///-------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] SceneReference m_UIScene;
    [SerializeField] SceneReference m_GameScene;
    [SerializeField] MenuClassifier m_EndGameScreen;
    [HideInInspector] public ChessBoard m_ChessBoard;
    [HideInInspector] public int m_WhiteRating = 2000;
    [HideInInspector] public int m_BlackRating = 2000;

    EndMenu m_EndMenu;

    void Awake()
    {
        LeanTween.init(1000);
    }

    void Start()
    {
        MultiSceneManager.Instance.mOnSceneLoad.AddListener(OnSceneLoad);
        MultiSceneManager.Instance.mOnSceneUnload.AddListener(OnSceneUnload);
        MultiSceneManager.Instance.LoadScene(m_UIScene);
    }

    void OnDestroy()
    {
        if (MultiSceneManager.IsValidSingleton())
        {
            MultiSceneManager.Instance.mOnSceneLoad.RemoveListener(OnSceneLoad);
            MultiSceneManager.Instance.mOnSceneUnload.RemoveListener(OnSceneUnload);
        }
    }


    void OnSceneLoad(List<string> pLoadedScenes)
    {
        //if (pLoadedScenes.Contains(m_UIScene))
        //{
        //    m_BlackRating = 2000;
        //    m_WhiteRating = 2000;
        //}
    }

    void OnSceneUnload(List<string> pUnloadedScenes)
    {
        if (pUnloadedScenes.Contains(m_UIScene))
        {
            MultiSceneManager.Instance.LoadScene(m_UIScene);
        }
    }


    public void StartGame()
    {
        MultiSceneManager.Instance.LoadScene(m_GameScene);
    }

    public void EndGame(Side _winner, Side _predicted)
    {
        MultiSceneManager.Instance.UnloadScene(m_GameScene);
        MenuManager.Instance.ShowMenu(m_EndGameScreen);
        if (m_EndMenu == null)
        {
            m_EndMenu = MenuManager.Instance.GetMenu<EndMenu>(m_EndGameScreen);
        }
        m_EndMenu.m_Winner.text = _winner.ToString();
        m_EndMenu.m_Predicted.text = _predicted.ToString();
    }

    public void ResetGame()
    {
        MultiSceneManager.Instance.UnloadScene(m_UIScene);
    }

}
