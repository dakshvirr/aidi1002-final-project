﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PawnSelectorMenu : Menu
{
    [HideInInspector] public ChessPiece m_Caller;
    [SerializeField] Sprite[] m_WhitePieces;
    [SerializeField] Sprite[] m_BlackPieces;
    [SerializeField] Image[] m_PieceImages;

    Side m_Chance;

    public void SetupMenu(Side _side)
    {
        MenuManager.Instance.ShowMenu(mMenuClassifier);
        m_Chance = _side;
        Sprite[] selectedArray = _side == Side.White ? m_WhitePieces : m_BlackPieces;
        m_PieceImages[0].sprite = selectedArray[(int)Piece.Rook];
        m_PieceImages[1].sprite = selectedArray[(int)Piece.Knight];
        m_PieceImages[2].sprite = selectedArray[(int)Piece.Bishop];
        m_PieceImages[3].sprite = selectedArray[(int)Piece.Queen];
        m_PieceImages[4].sprite = selectedArray[(int)Piece.Pawn];
    }

    public void SwapWith(int _piece)
    {
        MenuManager.Instance.HideMenu(mMenuClassifier);
        m_Caller.ChangePieceType((Piece)_piece, m_Chance == Side.White ? m_WhitePieces[(int)_piece] : m_BlackPieces[(int)_piece]);
    }
}
