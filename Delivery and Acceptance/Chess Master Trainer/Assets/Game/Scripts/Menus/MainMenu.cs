﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : Menu
{
    [SerializeField] InputField m_WhiteElo;
    [SerializeField] InputField m_BlackElo;
    [SerializeField] Button m_HowToPlay;
    [SerializeField] Button m_Play;

    void OnEnable()
    {
        OnValueChanged("");
    }

    public void OnValueChanged(string _val)
    {
        m_Play.interactable = !string.IsNullOrEmpty(m_WhiteElo.text) && !string.IsNullOrEmpty(m_BlackElo.text);
        if (m_Play.interactable)
        {
            GameManager.Instance.m_BlackRating = int.Parse(m_BlackElo.text);
            GameManager.Instance.m_WhiteRating = int.Parse(m_WhiteElo.text);
        }
    }

    public void StartGame()
    {
        MenuManager.Instance.HideMenu(mMenuClassifier);
        GameManager.Instance.StartGame();
    }

    public void HowToPlay(MenuClassifier _howToMenu)
    {
        MenuManager.Instance.ShowMenu(_howToMenu);
    }


}
