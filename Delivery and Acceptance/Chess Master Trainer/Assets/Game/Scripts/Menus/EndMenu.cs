﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndMenu : Menu
{
    public Text m_Winner;
    public Text m_Predicted;

    public void BackToMain()
    {
        GameManager.Instance.ResetGame();
    }

}
