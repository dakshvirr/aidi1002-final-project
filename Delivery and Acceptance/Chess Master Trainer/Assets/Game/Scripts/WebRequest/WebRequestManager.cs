﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class WebRequestManager : Singleton<WebRequestManager>
{
    [SerializeField] WebRequestConfig m_RequestConfig;

    [HideInInspector] public bool m_Initialized = false;

    [HideInInspector] public string m_Result = null;

    [HideInInspector] public bool m_Running = false;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        UnityWebRequest request = UnityWebRequest.Get(m_RequestConfig.m_Host + ":" + m_RequestConfig.m_Port.ToString());
        var asyncHandle = request.SendWebRequest();
        yield return new WaitUntil(() => asyncHandle.isDone);
        if (request.isNetworkError || request.isHttpError)
        {
            m_Initialized = false;
        }
        else
        {
            m_Initialized = true;
        }
    }

    public void SendPostRequest(string _path, string _data)
    {
        if (!m_Initialized)
        {
            return;
        }
        m_Result = null;
        m_Running = true;
        StartCoroutine(SendPostRequestCoroutine(_path, _data));
    }

    IEnumerator SendPostRequestCoroutine(string _path, string _data)
    {
        UnityWebRequest request = UnityWebRequest.Post(m_RequestConfig.m_Host + ":" + m_RequestConfig.m_Port.ToString() + _path, _data);
        var asyncHandle = request.SendWebRequest();
        yield return new WaitUntil(() => asyncHandle.isDone);
        if (request.isNetworkError || request.isHttpError)
        {
            yield break;
        }
        yield return new WaitUntil(() => request.downloadHandler.isDone);
        m_Result = request.downloadHandler.text;
        m_Running = false;
    }

}
