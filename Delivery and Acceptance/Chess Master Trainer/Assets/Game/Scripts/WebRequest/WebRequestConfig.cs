﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Web Request Config", menuName = "Web Requests/Request Config")]
public class WebRequestConfig : ScriptableObject
{
    [SerializeField] internal string m_Host;
    [SerializeField] internal int m_Port;
}
