#This file has some methods defined that are used for preprocessing in the evaluation


import numpy as np
import pandas as pd
import chess.pgn
import io
from IPython.display import display
from sklearn.preprocessing import LabelEncoder

# Getting dummies for nominal variables
def map_nominal_dummies(df, column_name):
    df_dummy = pd.get_dummies(df[column_name],prefix=column_name)
    df = pd.concat([df,df_dummy],axis=1)
    return df

def get_sample(series,total_sample_size,row_count):
    sample_size = (series.shape[0]/row_count) * total_sample_size
    sample_size = int(sample_size)
    return series.sample(sample_size)

def get_encoded_values(series):
    labelEnc = LabelEncoder()
    return labelEnc.fit_transform(series)

def get_df_sample(df, class_column, sample_size = 0.1):
    row_count = df.shape[0]
    total_sample_size = sample_size * row_count
    df = df.groupby(class_column).apply(get_sample,total_sample_size = total_sample_size, row_count = row_count)
    return df.reset_index(drop=True)

def get_dummies_for_all(df, list_of_nominal_columns, drop_columns = True):
    for column_name in list_of_nominal_columns:
        df = map_nominal_dummies(df,column_name)
    if drop_columns:
        df.drop(inplace=True,axis=1,columns=list_of_nominal_columns)
    return df.reset_index(drop=True)

def get_dummied_sample(df, list_of_nominal_columns, class_column, drop_columns = True, sample_size = 0.1):
    df = get_df_sample(df,class_column,sample_size)
    df = get_dummies_for_all(df,list_of_nominal_columns, drop_columns)
    return df.reset_index(drop=True)

def split_info_train_df(df, list_of_info_columns):
    info_df = df[list_of_info_columns].copy()
    train_columns = [x for x in df.columns if x not in list_of_info_columns]
    train_df = df[train_columns].copy()
    return (info_df,train_df)